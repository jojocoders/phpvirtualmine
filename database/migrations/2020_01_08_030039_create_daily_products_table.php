<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ore_id');
            //$table->foreign('ore_id')->references('id')->on('ores');
            $table->text('notes')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('supervised_by')->nullable();
            $table->date("cutoff_date")->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->index('ore_id');
            $table->unique(['ore_id','cutoff_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_products');
    }
}
