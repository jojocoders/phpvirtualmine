<?php

namespace App\Http\Controllers;

class DailyProductionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {

    }

    public function create(Request $request)
    {

    }

    public function show(Request $request, string $id)
    {

    }

    public function update(Request $request, string $id)
    {

    }

    public function destroy(Request $request, string $id)
    {

    }
}
