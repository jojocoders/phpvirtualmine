<?php

namespace App\Http\Controllers;
use App\Http\Validators\OreValidator;
use Illuminate\Http\Request;

class OreController extends Controller
{
    use OreValidator;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        return 'index';
    }

    public function create(Request $request)
    {
        $validator = $this->validateCreate($request);

        if ($validator->fails()) {
            return 'failed';
        }

        return 'create';
    }

    public function show(Request $request, string $id)
    {
        return 'show';
    }

    public function update(Request $request, string $id)
    {
        return 'update';
    }

    public function destroy(Request $request, string $id)
    {
        return 'destroy';
    }
}
