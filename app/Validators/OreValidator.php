<?php

namespace App\Http\Validators;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait OreValidator
{
    /**
     * Validate update user request input
     *
     * @param  Request $request
     * @throws \Illuminate\Auth\Access\ValidationException
     */
    protected function validateCreate(Request $request)
    {
        return Validator::make($request->all(), [
            'name'          => 'required|max:254',
            'description'   => 'required',
            'price'         => 'required|numeric',
            'discovered_at' => 'required|date'
        ]);
    }
}
