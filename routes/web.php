<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//examples
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/a/{aa}/b/{bb}', function ($aa, $bb) use ($router) {
    return $aa.$bb;
});

$router->get('/a/{aa}/c[/{cc}]', function ($aa, $cc=null) use ($router) {
    return $aa.$cc;
});

$router->get('/jewete', function ($aa, $cc=null) use ($router) {
    return $_ENV['JWT_SECRET_KEY'];
});

$router->group(['middleware' => 'jwt.auth','namespace' => 'Admin','prefix' => 'admin'], function() use ($router)
{
    $router->get('/', function () use ($router) {
        return 'admin';
    });
    $router->get('/nama', function () use ($router) {
        return 'nama admin';
    });

});

$router->group(['middleware' => 'jwt.auth', 'prefix' => 'ores'], function() use ($router) {
    $router->get(       '/',        'OreController@index');
    $router->post(      '/',        'OreController@create');
    $router->get(       '/{id}',    'OreController@show');
    $router->put(       '/{id}',    'OreController@update');
    $router->patch(     '/{id}',    'OreController@update');
    $router->delete(    '/{id}',    'OreController@destroy');
    // $router->options(   '[/{id}]',  'OreController@options');
});

$router->group(['middleware' => 'jwt.auth', 'prefix' => 'daily_productions'], function() use ($router) {
    $router->get(       '/',        'DailyProductionController@index');
    $router->post(      '/',        'DailyProductionController@create');
    $router->get(       '/{id}',    'DailyProductionController@show');
    $router->put(       '/{id}',    'DailyProductionController@update');
    $router->patch(     '/{id}',    'DailyProductionController@update');
    $router->delete(    '/{id}',    'DailyProductionController@destroy');
    // $router->options(   '[/{id}]',  'DailyProductionController@options');
});
